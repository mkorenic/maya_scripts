import maya.cmds as mc
import maya.mel as mel
   
 
class Skin_Utils(): 
 
     def __init__(self):
  
        """ Create a dictionary to store UI elements """
        self.UIElements = {}
          
        """ Check to see if the UI exists """
        self.windowName = "skinUtils"
        if mc.window(self.windowName, exists=True):
            mc.deleteUI(self.windowName)
         
        """ Define UI elements width and height """  
        self.windowWidth = 110
        self.windowHeight = 80
        buttonWidth = 100
        buttonHeight = 30
  
        """ Define a window"""
        self.UIElements["skinUtils"] = mc.window(self.windowName, width=self.windowWidth, height=self.windowHeight, title="skinUtils", sizeable=False,mxb = False, mnb = False)
          
        self.UIElements["rowColumnLayout"] = mc.rowColumnLayout( numberOfColumns=1, columnWidth=[(1, 110)], cs=[2, 10] )
         
        """ Use a flow layout for the  UI """
        self.UIElements["guiFlowLayout"] = mc.flowLayout(v=True, width=200, height=self.windowHeight, bgc=[0.4, 0.4, 0.4])
        mc.setParent(self.UIElements["rowColumnLayout"])
  
        self.UIElements["saveWeights"] = mc.button(label="Save", width=buttonWidth, height=buttonHeight, enable=True,  annotation='Save Skin Weights', p=self.UIElements["guiFlowLayout"], c=self.saveSkin)
        self.UIElements["loadWeights"] = mc.button(label="Load", width=buttonWidth, height=buttonHeight, enable=True,  annotation='Load Skin Weights', p=self.UIElements["guiFlowLayout"], c=self.loadSkin)
  
        """ Show the window"""
        mc.showWindow(self.windowName)
         
        
    def saveSkin(self, *args): 
       
        selection = self.getSelection()
        print selection
        if selection == []:
            return
        sknCluster = self.getSelectionSkinClusters(selection)
        print sknCluster
             
        verts = self.listVertices(selection)     
           
        mc.select( verts )
          
        # Add a file browser
                 
        basicFilter = "*.xml"
        fdir = mc.fileDialog2(fileFilter=basicFilter, dialogStyle=3)
        print fdir
          
        fileName = fdir[0].rpartition('/')[2]
        print fileName
          
        filePath = fdir[0].rpartition('/')[0]
        print filePath
          
        mc.deformerWeights (fileName,  ex=True, deformer=sknCluster, p=filePath)  
        mc.headsUpMessage( "Skin Exported" ) 
   
                   
    def getSelection(self, *args):
        # Identify the selection 
        sel = mc.ls(sl=True)
        if sel == []:
            mc.headsUpMessage( "Select Skinned Geometry" )
        return sel
               
           
    def getSelectionSkinClusters(self, selection, *args):
        #identify skinCluster 
        mel.eval ('$mesh = "%s";' % (selection[0]))
        skin = mel.eval('findRelatedSkinCluster($mesh);')
        if skin == False:
            mc.headsUpMessage('skinCluster Not Found')
        return skin
                
        
    def listVertices(self, selection, *args):
        #get vtx influences
        listVerts = (mc.ls ([selection[0]+'.vtx[*]'],flatten=True))
        print listVerts
        #check if the vtx wieghts were selected
        if selection in listVerts == True:
            print selection
  
 
    def loadSkin(self,selection,*args):
                      
        # open directory
        basicFilter = "*.xml"
        fdir = mc.fileDialog2(fileFilter=basicFilter,cap ='Import weights:',
        okCaption = 'Import', dialogStyle=2)
        print fdir
                  
        fileName = fdir[0].rpartition('/')[2]
        print fileName
                  
        filePath = fdir[0].rpartition('/')[0]
        print filePath 
        selection = self.getSelection()
         
        skinCluster = self.getSelectionSkinClusters(selection)
         
        #convert skinCluster to string
        sel = str(skinCluster)
         
        #import weights
        importSkin = mc.deformerWeights (fileName,im=True, deformer=sel, m='index',  p=filePath)
        if skinCluster == sel:
            mc.headsUpMessage(sel + ' Imported Successfully')
       
Skin_Utils()  